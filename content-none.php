<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Magnus
 */
?>

<?php 
	// wpml_current_language – Get the current display language
	$idioma_actual = apply_filters( 'wpml_current_language', NULL );
?>

<section class="no-results not-found">
	<header class="page-header">
		<h1 class="page-title"><?php 
			// Mensaje personalizado para mostrar el error de No Encontrado.
			switch ($idioma_actual) :
				case 'es': 	echo 'No Encontrado'; break;
				case 'en':	echo 'Not Found'; break;
				default: break; 
			endswitch; ?></h1>
	</header><!-- .page-header -->

	<div class="page-content"><?php 
		if ( is_search() ) : 
			// Dentro del template "search". Mensaje personalizado para mostrar el error de No Encontrado.
			switch ($idioma_actual) :
				case 'es': 	echo "<p>" . _e( 'Lo sentimos, pero no hay ninguna coincidencia para ese término de búsqueda. Por favor, inténtelo de nuevo con algunas palabras clave diferentes.', 'bnelab' ) . "</p>";		break;
				case 'en':	echo "<p>" . _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'bnelab' ) . "</p>";		break;
				default:	break;
			endswitch;

		get_search_form(); ?>

		<?php else : ?>
			<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'magnus' ); ?></p>
			<?php get_search_form(); ?>
		<?php endif; ?>
	</div><!-- .page-content -->
</section><!-- .no-results -->
