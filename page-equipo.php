 <?php
/**
 * The template used for displaying page content from page-equipo.php
 *
 * Template Name: Template Equipo (BNElab)
 * @author 	Felipe Rodríguez (Serikat)
 * @version 9.0
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php 
			/* Plantilla usada para la Información y Perfiles de contacto del Equipo BNElab.
			*  Haciendo uso del ACF Módulo: Equipo BNElab.
			*/
			get_template_part('inc/content','equipo'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->	
<?php get_sidebar(); ?>
<?php get_footer(); ?>