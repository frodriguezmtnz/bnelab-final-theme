<?php
/**
 * Plantilla usada para el contenido de las Taxonomias del CPT Dato
 * 
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<?php
// wpml_current_language – Get the current display language
$idioma_actual = apply_filters( 'wpml_current_language', NULL );
?>

<!-- ul-columns-taxonomies -->
<ul data-aos="fade-up" data-aos-anchor-placement="center-bottom" data-aos-duration="900" data-aos-once="true" class="columns-taxo">

	<!-- Taxonomia de Licencias -->
	<li class="taxo-one">
		<h2 class="title-taxo"><i class="fa fa-creative-commons" aria-hidden="true"></i> <?php switch ($idioma_actual) {
			case 'es': echo 'Licencias'; break;
			case 'en': echo 'Licenses';   break;
			default: break;
		} ?></h2>
		<div class="body-taxo"><?php  // Taxonomia Licencias
			$wcatTerms = wp_get_post_terms(get_the_ID(), 'licencia', array( 'orderby' => 'name', 'order' => 'ASC' ) );
			//var_dump($wcatTerms);
			if (!empty($wcatTerms)):
			   foreach($wcatTerms as $wcatTerm) : ?>			   		
			         <?php
			         	if ($wcatTerm->parent != 0 ): ?>
							<a class="subtaxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php else: ?>
							<a class="taxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php endif;
			   endforeach;
			endif; 		?>
		</div>
	</li>

	<!-- Taxonomia de Materiales -->
	<li class="taxo-two">
		<h2 class="title-taxo"><i class="fa fa-archive" aria-hidden="true"></i> <?php switch ($idioma_actual) {
			case 'es': echo 'Materiales'; break;
			case 'en': echo 'Materials';   break;
			default: break;
		} ?></h2>
		<div class="body-taxo"><?php  // Taxonomia Materiales
			$wcatTerms = wp_get_post_terms(get_the_ID(), 'material', array( 'orderby' => 'name', 'order' => 'ASC' ) );			
			if (!empty($wcatTerms)):
			   foreach($wcatTerms as $wcatTerm) : ?>			   		
			         <?php
			         	if ($wcatTerm->parent != 0 ): ?>
							<a class="subtaxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php else: ?>
							<a class="taxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php endif;
			   endforeach;
			endif; 		?>
		</div>
	</li>

	<!-- Taxonomia de Formatos -->
	<li class="taxo-three">
		<h2 class="title-taxo"><i class="fa fa-file-code-o" aria-hidden="true"></i> <?php switch ($idioma_actual) {
			case 'es': echo 'Formatos'; break;
			case 'en': echo 'File Format';   break;
			default: break;
		} ?></h2>
		<div class="body-taxo"><?php  // Taxonomia Formato
			$wcatTerms = wp_get_post_terms(get_the_ID(), 'formato', array( 'orderby' => 'name', 'order' => 'ASC' ) );			
			if (!empty($wcatTerms)):
			   foreach($wcatTerms as $wcatTerm) : ?>			   		
			         <?php
			         	if ($wcatTerm->parent != 0 ): ?>
							<a class="subtaxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php else: ?>
							<a class="taxo-link" href="<?php echo get_term_link( $wcatTerm->slug, $wcatTerm->taxonomy ); ?>"><?php echo $wcatTerm->name; ?></a>
			         	<?php endif;
			   endforeach;
			endif; 		?>
		</div>
	</li>

	<!-- Datos Relacionados -->
	<li class="taxo-four">
		<h2 class="title-taxo"><i class="fa fa-sitemap" aria-hidden="true"></i> <?php switch ($idioma_actual) {
			case 'es': echo 'Relacionados'; break;
			case 'en': echo 'Related';   break;
			default: break;
		} ?></h2>

<?php 	if ( !empty (get_field('elegir-datos-relacionados')) ):  // se muestra SOLO datos si no está vacío. ?>
		<div class="body-taxo">
<?php
		//echo "Entro en la condicion MODULO ACF datos relacionados";
		$post_objects = get_field('elegir-datos-relacionados');
		/* -- Módulo: Datos relacionados y contacto (BNElab)
		Nos recorremos el array de objetos del CPT 'dato', y mostramos
		los datos relacionados con el actual post Dato que se está editando.
		Nos vale solo con hacer un listado de <ul><li> por CSS sencillo.
		*/
		foreach( $post_objects as $post): // variable must be called $post (IMPORTANT)
			setup_postdata($post); //preparamos los datos de listado de datos relacionados en BNElab ?>
				<a class="data-related" href="<?php the_permalink(); ?>" title="<?php esc_html (the_title());?> - <?php bloginfo ('name'); ?>"><?php echo esc_html( the_title() ); ?></a>
<?php endforeach; wp_reset_postdata();?>
		</div>

<?php endif; ?>
	</li>

	<!-- Modulo ACF Contacto para Dato en taxonomía -->
	<li class="taxo-five">
		<h2 class="title-taxo"><i class="fa fa-envelope" aria-hidden="true"></i> <?php switch ($idioma_actual) {
			case 'es': echo 'Contacto'; break;
			case 'en': echo 'Contact';   break;
			default: break;
		} ?></h2>
<?php if (!empty (get_field('contacto-asociado-dato')) ): ?>		
		<div class="body-taxo">			
			<a href="mailto:<?php echo get_field('contacto-asociado-dato'); ?>"><?php echo get_field('contacto-asociado-dato'); ?></a>
		</div>
<?php endif; ?>		
	</li>	
</ul><!-- ./ul-columns-taxonomies -->