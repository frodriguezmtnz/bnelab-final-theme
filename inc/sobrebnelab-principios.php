<?php
/**
 * Platilla usada sobre para mostar el Slider de Carrusel
 * para los Principios BNElab que se alimentan de ACF + Swiper (js+css)
 *
 * @package Magnus
 */
?>
	<!-- Carousel BNElab + WP -->
		<?php //echo ("Entro inc/sobrebnelab-principios.php"); ?>
		<div class="swiper-container">
		    <!-- Additional required wrapper -->
		    <div class="swiper-wrapper">
		        <!-- Slides -->
				<?php 
				$number_slide = 0;
				if ( have_rows('informacion-slider') ):
               		while ( have_rows('informacion-slider') ): the_row();
         				if ( get_row_layout() == 'slide' ): $number_slide++; ?>
							<div class="swiper-slide overlay" style="background:url(<?php the_sub_field('imagen-fondo-slide'); ?>) center center no-repeat; min-height: 400px; background-size: cover;">
								<div class="content-slider">
									<div class="number-slide"><?php echo " 0" . $number_slide; ?></div>
									<div class="bloque-centrado">
										<div class="title"><?php the_sub_field('titulo-slide'); ?></div>
										<div class="text"><?php the_sub_field('contenido-texto-slide'); ?></div>
									</div>
								</div>
							</div>
<?php 					endif;
                	endwhile;
                endif;
?>						
			</div>
			<!-- If we need pagination -->
			<div class="swiper-pagination"></div>
			 
			<!-- If we need navigation buttons -->
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
			 
			<!-- If we need scrollbar -->
			<div class="swiper-scrollbar"></div>
		</div>