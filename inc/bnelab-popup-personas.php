<?php
/**
 * Plantilla usada para mostrar el campo "Descripción Larga (Equipo/Colaborador)" ACF,
 * de forma separada de la anterior plantilla. Así se puede llamar a esta plantilla
 * "get_template_part('inc/perfiles-sociales','personas');", pasándole por referencia las variables almacenadas
 * para así recorrerlas e ir montando y mostrando los popups para cada persona.
 *
 * Se alimenta del ACF Módulo: Colaboradores & Equipo BNElab -> contenido flexible "Descripción Larga (Equipo)"
 * 
 */
?>
	<?php 

		// Extracción de los valores de las variables por referencias a otras para poder usarlas.
		$descripcion_larga = get_query_var('descripcion_larga'); //recuperamos el valor "descripcion_larga"
		$nombre_array = get_query_var('nombre_array'); //recuperamos el valor "nombre_array"
		$imagen_array = get_query_var('imagen_array'); //recuperamos el valor "imagen_array"
		$bnelab_popup = get_query_var('bnelab_popup'); //recuperamos el valor "bnelab_popup"	

		/*
		* Montamos cada popup_nº con la información de cada persona: foto, nombre y descripcion larga.
		* Para ello, nos recorremos el array según el número de veces que se ha rellenado el campo 'descripcon larga'.
		* Además, para cada popup, se ha personalizado su jquery individual + css que engloba a todos los popups.
		*/

		$i=1;  //puntero para movernos por el array.
		while ($i <= sizeof($descripcion_larga)): ?>

		<div id="<?php echo $bnelab_popup[$i]; ?>">
			<?php //echo "inc/plantilla bnelab-popup-personas.php"; ?>
			<img class="img-colaboradores-popup" src="<?php echo $imagen_array[$i]; ?>" />
			<h3 class="popup"><?php echo $nombre_array[$i]; // mostramos el contenido del campo 'nombre' ?></h3>
			<?php echo $descripcion_larga[$i]; // mostramos el contenido del campo 'descripcion_larga' ?>
			<a class="<?php echo $bnelab_popup[$i].'_close'; ?>"><i class="fa fa-times-circle fa-4x"></i></a>
		</div>

		<script>
			jQuery(document).ready(function () {
				$(<?php echo "'#".$bnelab_popup[$i]."'"; ?>).popup({
				color: '#403f3f',
				opacity: 1,
				transition: 'all 0.3s',
				scrolllock: false, 
				escape: true,
				vertical: 'top',
				horizontal: 'center'
				});
			});
		</script>

		<style>
			<?php echo "#".$bnelab_popup[$i].""; ?> {
				-webkit-transform: scale(0.8);
				-moz-transform: scale(0.8);
				-ms-transform: scale(0.8);
				transform: scale(0.8);
			}

			.popup_visible <?php echo "#".$bnelab_popup[$i].""; ?> {
				-webkit-transform: scale(1);
				-moz-transform: scale(1);
				-ms-transform: scale(1);
				transform: scale(1);
				margin: 10vh;
				color: white;
			}

			a.<?php echo $bnelab_popup[$i].'_close'; ?>{
				text-align: center;
				margin: 0 auto;
				display: block;
				color: white;
				opacity: 0.6;
			}
		</style>

<?php 		$i++; //aumentamos el contador del puntero para movernos por el array.
		endwhile; ?>