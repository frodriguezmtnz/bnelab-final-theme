<?php
/**
 * The template used for displaying page content from page-sobrebnelab.php
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		
		<div class="entry-content">
			<?php 
			/* Slider con los Principios en page "Sobre BNElab" + ACF
			 * ------------------------------------------------------
			 * NOTA: Sólo carga en el header.php, ya que se muestra
			 * debajo del header, antes del content de esta página.
				if (is_page('sobre-bnelab')):
					get_template_part('inc/sobrebnelab','principios');
				endif;
			*/
			?>

			<?php 
				// Cargamos la plantilla asociada a los breadcrumb para cualquier tipo de Page.
				get_template_part('inc/breadcrumb-equipo','colaboradores'); 
			?>
		<?php
		// wpml_current_language – Get the current display language
		$idioma_actual = apply_filters( 'wpml_current_language', NULL );
		// ICL_LANGUAGE_CODE --> https://wpml.org/documentation/support/wpml-coding-api/

		// Extraigo el valor del campo "Cadena texto Equipo" & "Cadena texto Colaboradores" & "Cadena texto Contacto".
		$cadena_texto_equipo 		= get_field("cadena-texto-equipo");
		$cadena_texto_colaborador 	= get_field("cadena-texto-colaborador");
		$cadena_texto_contacto 		= get_field("cadena-texto-contacto");

		// Extraigo el objeto asociado al enlace de pág. ya creado para "Equipo" y "Colaboradores" (es y en),
		// para posteriormente hacer un get_the_permalink y get_the_title para cada ID.
		$equipo_object			= get_field('enlace-equipo-bnelab', false, false);
		$colaboradores_object 	= get_field('enlace-colaboradores-bnelab', false, false); 
		?>
		<ul class="columns">
			<li data-aos="fade-right" data-aos-duration="800" data-aos-once="true" class="wide-one">
		    	<h2 class="title-module"><i class="fa fa-users"></i><?php
					// Dependiendo del idioma y si no está vacío la cadena de texto del Equipo, lo mostramos
					if ( ($idioma_actual == ICL_LANGUAGE_CODE ) && ( !(empty($cadena_texto_equipo)) || !(empty($cadena_texto_colaborador)) || !(empty($cadena_texto_contacto)) ) ):
						echo $cadena_texto_equipo;
					else: // si no, cargamos los textos por defecto según idioma.
						switch ($idioma_actual) {
							case 'es': echo 'Equipo BNElab'; break;
							case 'en': echo 'Team BNElab';   break;
							default: break;
						}
					endif;
		    	?></h2>
		    	<div class="body"><?php the_field('descripcion-equipo-bnelab'); ?></div>
		    	<a class="about-bnelab-link" href="<?php echo esc_html( get_the_permalink($equipo_object) ); ?>" title="Info. <?php echo esc_html( get_the_title($equipo_object) ); ?> BNElab"><div class="cta"><i class="fa fa-link"></i><?php echo esc_html( get_the_title($equipo_object) ); ?> BNElab</div></a>
		  	</li>
		   	<li data-aos="fade-left" data-aos-duration="800" data-aos-once="true" class="wide-two">
		     	<h2 class="title-module"><i class="fa fa-street-view"></i><?php
					// Dependiendo del idioma y si no está vacío la cadena de texto del Colaborador, lo mostramos
					if ( ($idioma_actual == ICL_LANGUAGE_CODE ) && ( !(empty($cadena_texto_equipo)) || !(empty($cadena_texto_colaborador)) || !(empty($cadena_texto_contacto)) ) ):
						echo $cadena_texto_colaborador;
					else: // si no, cargamos los textos por defecto según idioma.
						switch ($idioma_actual) {
							case 'es': echo 'Colaboradores'; break;
							case 'en': echo 'Collaborators';   break;
							default: break;
						}
					endif;
		    	?></h2>
		     	<div class="body"><?php the_field('descripcion-colaboradores-bnelab'); ?></div>
		     	<a class="about-bnelab-link" href="<?php echo esc_html( get_the_permalink($colaboradores_object) ); ?>" title="Info. <?php echo esc_html( get_the_title($colaboradores_object) ); ?> BNElab"><div class="cta"><i class="fa fa-link"></i><?php echo esc_html( get_the_title($colaboradores_object) ); ?></div></a>
		   	</li>
		   	<li data-aos="fade-up" data-aos-duration="800" data-aos-once="true" class="contact">
		     	<h3 class="title-module"><i class="fa fa-street-view"></i><?php
					// Dependiendo del idioma y si no está vacío la cadena de texto del Contacto, lo mostramos
					if ( $idioma_actual == ICL_LANGUAGE_CODE  && ( !(empty($cadena_texto_equipo)) || !(empty($cadena_texto_colaborador)) || !(empty($cadena_texto_contacto)) ) ):
						//echo ICL_LANGUAGE_CODE;
						echo $cadena_texto_contacto;
					else: // si no, cargamos los textos por defecto según idioma.
						switch ($idioma_actual) {
							case 'es': echo 'Contacto'; break;
							case 'en': echo 'Contact';   break;
							default: break;
						}
					endif;
		    	?></h3>
		     	<div class="body-contact"><?php the_field('descripcion-contacto-bnelab'); ?></div>
		     	<div class="cta-contact"><a href="mailto:<?php esc_html( the_field('contacto-email-bnelab') ); ?>" title="Contacta con BNElab por email"><i class="fa fa-envelope"></i><?php esc_html( the_field('contacto-email-bnelab') ); ?></a></div>
		   	</li>
		</ul><!-- /.columns -->
			
		</div><!-- .entry-content home-->
		<?php get_template_part('inc/page','social'); ?>
</article><!-- #post-## -->