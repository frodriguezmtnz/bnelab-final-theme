<?php
	/* 
	* https://codex.wordpress.org/Function_Reference/get_post_type_object
	* https://wordpress.stackexchange.com/questions/169504/how-to-get-current-get-post-types-name
	* Según el tipo de objeto, ya sea post, page, o CPTs, nos quedamos con el 
	* singular_name asociado a ese objeto. Luego guardamos el string en una variable,
	* y lo usamos en el switch para hacer el Padre de cada CPT.
	*/
?>

	<div class="entry-meta" typeof="BreadcrumbList" vocab="https://schema.org/">
		<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="BNElab Home"><i class="fa fa-home fa-3" aria-hidden="true"></i>BNElab</a>
							
<?php
	// Siempre al menos, mostramos la casa de inicio para que la navegación sea consecuente.

	global $post; //para acceder a los datos

	// Solo mostramos esta parte si cumple la condición de ser página hija y tener un padre
	if ( is_page() && $post->post_parent ):
		//echo "&nbsp;&raquo;&nbsp;"; 
		echo "<span class='separator'>&raquo;</span>";
		echo '<a href="'. get_permalink($post->post_parent).'" title="'. get_the_title($post->post_parent) .' " >'. apply_filters('the_title', get_the_title($post->post_parent)) .'</a>'; //mostramos y extraemos tanto el nombre, url como title del padre.
	endif;
?>

	</div> <!-- /.entry-meta -->