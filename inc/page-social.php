<?php
/**
 * The template used for social sharing in bottom page/single for WHOLE web page BNElab.
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<?php 
	global $post;

	// wpml_current_language – Get the current display language
	$idioma_actual = apply_filters( 'wpml_current_language', NULL );

    // Get current page URL 
    $socialURL = urlencode(get_permalink());
 
    // Get current page title
    $socialTitle = str_replace( ' ', '%20', get_the_title());

    // Get current excerpt page
	$socialExcerpt = limitar_palabras(get_the_excerpt(), '55');
        
	// Get Post Thumbnail for pinterest
	$socialThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' );

	$content = null;

	/* We check what language is active, for customizing texts social network.
	* --- Spanish language --- */
	if ($idioma_actual == 'es'):
		// Construct sharing URL without using any script
	    $twitterURL  = 'https://twitter.com/intent/tweet?text='.$socialExcerpt.'&amp;url='.$socialURL.'&amp;via=BNElab';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$socialURL;
		$googleURL   = 'https://plus.google.com/share?url='.$socialURL;
		$emailURL    = 'mailto:?subject=' . $socialTitle . '&body=' .$socialTitle. ' | BNElab: ' .$socialExcerpt. ' Web &raquo; ' .$socialURL .'" title="Compartido por BNElab.';
		$whatsappURL = 'whatsapp://send?text='.$socialExcerpt . ' ' . $socialURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$socialURL.'&amp;title='.$socialExcerpt;
		$rssURL = get_feed_link();
	 
		// Add sharing button at the end of page/page content
		$content .= '<div data-aos="fade-left" data-aos-duration="800" data-aos-once="true" class="social-bnelab">';
		$content .= '<i class="fa fa-share-alt fa-2 sharing-lab" aria-hidden="true"></i>';
		$content .= '<a rel="noopener noreferrer" title="RSS BNElab" class="social-link social-rss"      href="'.$rssURL.'" target="_blank"><i class="fa fa-rss fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Compártelo por Email" class="social-link social-email"    href="'.$emailURL.'" target="_blank"><i class="fa fa-envelope-o fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Compártelo en Twitter" class="social-link social-twitter"  href="'.$twitterURL.'"  target="_blank"><i class="fa fa-twitter  fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Compártelo en Facebook" class="social-link social-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Compártelo en LinkedIn" class="social-link social-linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fa fa-linkedin fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Compártelo en Whatsapp" class="social-link social-whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="fa fa-whatsapp fa-2" aria-hidden="true"></i></a>';    
		$content .= '</div>';
	endif;

	/* We check what language is active, for customizing texts social network.
	* --- English language --- */
	if ($idioma_actual == 'en'):
		// Construct sharing URL without using any script
	    $twitterURL  = 'https://twitter.com/intent/tweet?text='.$socialExcerpt.'&amp;url='.$socialURL.'&amp;via=BNElab';
		$facebookURL = 'https://www.facebook.com/sharer/sharer.php?u='.$socialURL;
		$googleURL   = 'https://plus.google.com/share?url='.$socialURL;
		$emailURL    = 'mailto:?subject=' . $socialTitle . '&body=Project BNElab: ' .$socialExcerpt. ' Web &raquo; ' .$socialURL .'" title="Shared by BNElab.';
		$whatsappURL = 'whatsapp://send?text='.$socialExcerpt . ' ' . $socialURL;
		$linkedInURL = 'https://www.linkedin.com/shareArticle?mini=true&url='.$socialURL.'&amp;title='.$socialExcerpt;
		$rssURL = get_feed_link();
	 
		// Add sharing button at the end of page/page content
		$content .= '<div data-aos="fade-left" data-aos-duration="800" data-aos-once="true" class="social-bnelab">';
		$content .= '<i class="fa fa-share-alt fa-2 sharing-lab" aria-hidden="true"></i>';
		$content .= '<a rel="noopener noreferrer" title="RSS BNElab" class="social-link social-rss"      href="'.$rssURL.'" target="_blank"><i class="fa fa-rss fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Share it by Email" class="social-link social-email"    href="'.$emailURL.'" target="_blank"><i class="fa fa-envelope-o fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Share it by Twitter" class="social-link social-twitter"  href="'.$twitterURL.'"  target="_blank"><i class="fa fa-twitter  fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Share it by Facebook" class="social-link social-facebook" href="'.$facebookURL.'" target="_blank"><i class="fa fa-facebook fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Share it by LinkedIn" class="social-link social-linkedin" href="'.$linkedInURL.'" target="_blank"><i class="fa fa-linkedin fa-2" aria-hidden="true"></i></a>';
		$content .= '<a rel="noopener noreferrer" title="Share it by Whatsapp" class="social-link social-whatsapp" href="'.$whatsappURL.'" target="_blank"><i class="fa fa-whatsapp fa-2" aria-hidden="true"></i></a>';    
		$content .= '</div>';
	endif;
 
	echo $content;
?>