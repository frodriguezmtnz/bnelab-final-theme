<?php
/**
 * Extra coding feature template for section Feature project, tools, data...
 * 1) Multi-CPT for feature section on page:
 *   1.1) "Módulo: Listado de Proyectos (BNElab)"
 *   1.2) "Módulo: Listado de Herramientas (BNElab)"
 *   1.3) "Módulo: Listado de Datos (BNElab)"
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 * @version 9.0
 *
 */
?>
		<?php
		/*
		* Inicializamos los objetos posts, para hacer uso del "Módulo ACF Módulo: Listado de Proyectos, Herramientas y Datos (BNElab)"
		* En este módulo, tiene campos multiselección. Usamos set_postdata para hacer uso
		* de las funciones de WP core nativas de forma normal.
		* http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
		*/				

		$slug = get_queried_object()->post_name;  // Cogemos el nombre del slug de la página.

		/*
		* Según el slug de la página según el idioma del CPT, lo fuerzo al idioma español,
		* para tener el mismo código reutilizable en las llamadas a los módulos ACF.
		*/
		switch ($slug) {
		 	case 'projects': $slug = 'proyectos'; 	  break;
		 	case 'data':     $slug = 'datos';         break;
		 	case 'tools':    $slug = 'herramientas';  break;
		 	default:	break;
		 } 

		$acf_custom = $slug . '-bnelab';          // Unimos el slug con -bnelab para formar el ACF del módulo.
		$post_objects = get_field($acf_custom);   // Llamada al módulo para inicializarlo.

		// Solo si tiene objetos el listado del módulo, entramos y lo recorremos.
		if( !empty($post_objects) ): ?>
		<div class="container">
				<div class="parent">			
				<?php
					// Inicializacion para libreria para detectar el user-agent de movil/tablet/desktop
					require_once 'mobile-detect/Mobile_Detect.php';
					$detect = new Mobile_Detect;

					/* Nos recorremos el array de objetos del CPT, y mostramos mediante,
					flex-box-grid los elementos de forma ordenada según los hijos. Todo por CSS. */
				    foreach( $post_objects as $post): // variable must be called $post (IMPORTANT)
		        			setup_postdata($post);    //preparamos los datos de listado de proyectos hechos en BNElab
		        			if( has_post_thumbnail() ): 
							$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
									<div data-aos="zoom-in" data-aos-duration="800" data-aos-once="true" class="child">
										<figure class="grid-proyectos">
											<img class="img-fluid" src="<?php echo $attachment_image[0]; ?>" alt="imagen <?php the_title(); ?>" />
											<figcaption>
												<h3><?php the_title(); ?></h3>
												<p><?php 										
													if ( $detect->isMobile() || $detect->isTablet() ): //Movil+Tablet con excerpt recortado si manual o automatico
														if (has_excerpt()): echo limitar_palabras(get_the_excerpt(), '15'); echo "... &raquo;"; else: echo limitar_palabras(get_the_excerpt(), '15'); echo "... &raquo;"; endif;
													else: //Desktop con excerpt recortado si manual o automatico
														if (has_excerpt()): echo limitar_palabras(get_the_excerpt(), '40'); echo "... &raquo;"; else: echo limitar_palabras(get_the_excerpt(), '40'); echo "... &raquo;"; endif;
													endif;
												?></p>
											</figcaption>
												<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"></a>
										</figure>
									</div>
				<?php 		else: //Si no hay Imagen destacada para un proyecto CPT, cargamos la imagen "proyectos-bnelab.jpg" ?>
									<div data-aos="zoom-in" data-aos-duration="800" data-aos-once="true" class="child">
										<figure class="grid-proyectos">
										<img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/images/proyectos-bnelab.jpg" alt="imagen <?php the_title(); ?>" />
											<figcaption>
												<h3><?php the_title(); ?></h3>
												<p><?php 										
													if ( $detect->isMobile() || $detect->isTablet() ): //Movil+Tablet con excerpt recortado si manual o automatico
														if (has_excerpt()): echo limitar_palabras(get_the_excerpt(), '15'); echo "... &raquo;"; else: echo limitar_palabras(get_the_excerpt(), '15'); echo "... &raquo;"; endif;
													else: //Desktop con excerpt recortado si manual o automatico
														if (has_excerpt()): echo limitar_palabras(get_the_excerpt(), '40'); echo "... &raquo;"; else: echo limitar_palabras(get_the_excerpt(), '40'); echo "... &raquo;"; endif;
													endif;
												?></p>
											</figcaption>
												<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"></a>
										</figure>
									</div>
				<?php
							endif;
						endforeach; wp_reset_postdata(); ?>
				</div><!-- /.parent -->
		</div><!-- /.container -->
		<?php endif; ?>