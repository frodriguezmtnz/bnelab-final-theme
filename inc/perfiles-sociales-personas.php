<?php
/**
 * Plantilla usada para los Perfiles Sociales de las Personas (hija del padre Sobre-BNElab)
 * Se alimenta del ACF Módulo: Colaboradores & Equipo BNElab -> contenido flexible "Contactar con el Colaborador/Equipo"
 * 
 */
?>

<?php 
	/* Sección de perfiles de contacto: Contactar con el Colaborador
	*  Contenido flexible donde se rellena como máximo un perfil por persona:
	*  perfil colaborador con: Email, LinkedIn y/o Twitter. Sólo obligatorio email.
	*  Resto de perfiles son opcionales.
	*/

	$slug = get_queried_object()->post_name;  // Cogemos el nombre del slug de la página.
	$acf_custom_people = 'contactos-'.$slug;  // Unimos el slug con -bnelab para formar el ACF del módulo.

	//echo $slug . " \n \n";
	//echo  $acf_custom_people . "\n";

	if ( have_rows($acf_custom_people) ):
		while ( have_rows($acf_custom_people) ): the_row();
		// Si tiene filas, contiene al menos un perfil, como el Email.
		// Por cada layout, comprobamos que no esté vacío el campo para poder mostrarlo.
			if (get_row_layout() == 'perfiles-de-contacto' ):
			// info: https://developer.wordpress.org/reference/functions/set_query_var/
			$nombre = get_query_var('name_people'); //recuperamos el valor que hay en el 2º valor de la funcion "set_query_var", pero con get.
				if (!empty(get_sub_field('email-'.$slug))): ?>
					<span class="hint--bottom hint--rounded hint--bounce" aria-label="Email <?php echo $nombre; ?>"><a href="mailto:<?php the_sub_field('email-'.$slug); ?>"><i class="fa fa-envelope fa-2x" aria-hidden="true"></i></a></span>
		<?php   endif;
				if (!empty(get_sub_field('linkedin-'.$slug))): ?>
					<span class="hint--bottom hint--rounded hint--bounce" aria-label="LinkedIn <?php echo $nombre; ?>"><a href="<?php the_sub_field('linkedin-'.$slug); ?>"><i class="fa fa-linkedin fa-2x" aria-hidden="true"></i></a></span>
		<?php   endif;
				if (!empty(get_sub_field('twitter-'.$slug))): ?>
					<span class="hint--bottom hint--rounded hint--bounce" aria-label="Twitter <?php echo $nombre; ?>"><a href="<?php the_sub_field('twitter-'.$slug); ?>"><i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></span>
		<?php   endif;
			endif;// /.endif perfiles-de-contacto
										
		endwhile; // /.endwhile 'contactos-colaboradores/equipo'
	endif; // /.endif 'contactos-colaboradores/equipo'
?>