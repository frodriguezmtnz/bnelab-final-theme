<?php
/**
 * Plantilla usada para la página Equipo (hijo del padre Sobre-BNElab)
 * 
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		<header class="entry-header">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php
				// Cargamos la plantilla asociada a los breadcrumb para cualquier tipo de Page.
				get_template_part('inc/breadcrumb-equipo','colaboradores'); 
			?>			
		</header><!-- .entry-header -->
		<div class="entry-content">
			<?php while ( have_posts() ) : the_post(); 				
				the_content(); // necesario hacer el while para mostrar el content
			 endwhile; // end of the loop. ?>
				<ul class="listing"><?php 
			 	/* Nos recorremos contenido flexible del módulo ACF "Módulo: Colaboradores BNElab"
				*  Un Colaborador consta de: Foto, Nombre, Descripcion y Perfiles de contacto (email, TW, FB y LinkedIn)
				*/
				if ( have_rows('equipo-bnelab') ):
					$i=0;
               		while ( have_rows('equipo-bnelab') ): $i++; the_row();
               			/* Si hay al menos una persona del equipo relleno, obtenemos los datos,
						*  para mostrarlo en el front-end. Perfiles de contacto limitado a 1 por persona.
						*/
         				if ( get_row_layout() == 'equipo' ): ?>
						<li data-aos="zoom-out" data-aos-duration="800" data-aos-once="true">
							<?php $imagen=get_sub_field('foto-equipo'); //array con la imagen ?>
							<img class="img-colaboradores" src="<?php the_sub_field('foto-equipo'); ?>" alt="Equipo BNElab - <?php the_sub_field('nombre-colaborador'); ?>" />
							<div class="name"><h2><?php $nombre=get_sub_field('nombre-equipo'); echo $nombre; ?></h2></div>
							<div class="body"><p><?php the_sub_field('descripcion-equipo'); ?></p></div>
							<?php 
								$descripcion_larga[$i]=get_sub_field('descripcion-larga-equipo');   //array con el campo "descripcion larga"
								$nombre_array[$i] = $nombre;	// array con el nombre de la persona
								$imagen_array[$i] = $imagen;	// array con la imagen de la persona
								$bnelab_popup[$i] = "popupbnelab_".$i;	// array con el nombre para el popup

								// wpml_current_language – Get the current display language
								$idioma_actual = apply_filters( 'wpml_current_language', NULL );
								switch ($idioma_actual) {
									case 'es': $info_more = 'Información ampliada de ';		break;
									case 'en': $info_more = 'Expanded information of ';		break;
									default:	break;
								}				
							?>	

							<div class="icon_bnelabpopup">
								<span class="hint--top hint--rounded hint--bounce" aria-label="<?php echo "$info_more $nombre"; ?>">
									<a class="<?php echo $bnelab_popup[$i]."_open"; ?>"><i class="fa fa-plus-circle fa-3x"></i></a>
								</span>
							</div>

							<div class="social">
								<?php 
									/* Plantilla usada para los Perfiles Sociales de las Personas, asociada
									*  al ACF Módulo: Equipo BNElab.
									*/
									set_query_var ('name_people', $nombre);  //pasamos una variable al template part
									// info: https://developer.wordpress.org/reference/functions/set_query_var/
									get_template_part('inc/perfiles-sociales','personas'); //cargamos el template part + variable 
								?> 
							</div>
						</li>


<?php 					endif; // /.endif 'equipo'
					 endwhile; // /.endwhile 'equipo-bnelab'
				 endif; ?>
				</ul><!-- /.listing -->
		</div><!-- .entry-content home-->


			<?php

			/* Plantilla usada para crear los PopUps del campo adicional "Descripción Larga (Equipo)",
			*  del ACF Módulo: Equipo BNElab.
			*  Paso por referencia las variables_array, y construir allí los popup.
			*  info: https://developer.wordpress.org/reference/functions/set_query_var/
			*/

			set_query_var ('descripcion_larga', $descripcion_larga);
			set_query_var ('nombre_array', $nombre_array);
			set_query_var ('imagen_array', $imagen_array);
			set_query_var ('bnelab_popup', $bnelab_popup);
			get_template_part('inc/bnelab-popup','personas'); ?>

		<?php get_template_part('inc/page','social'); ?>
</article><!-- #post-## -->