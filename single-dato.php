<?php
/**
 * Esta plantilla carga el contenido asociado a un CPT "Dato BNElab" más (+)
 * taxonomías asociadas para ese tipo de Datos: licencias, materiales, formatos, datos relacionados y contacto.
 * Para ello, se ha creado una plantilla adicional para separar la lógica de: Taxonomías y ACF. 
 * 
 * @package bnelab-theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<a href="#top" id="toTop">Subir arriba</a>
		<main id="main" class="site-main" role="main">

			<?php global $post; while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
					<?php 
					// Text-justify center ONLY for home page. ?>
					<header class="entry-header">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

						<div class="entry-meta">
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="BNElab Home"><i class="fa fa-home fa-3" aria-hidden="true"></i>BNElab</a>
						<?php
						/* Migas de pan para el tipo de dato "Datos BNElab"
						* https://codex.wordpress.org/Function_Reference/get_post_type_object
						* https://wordpress.stackexchange.com/questions/169504/how-to-get-current-get-post-types-name
						* Según el tipo de objeto, ya sea post, page, o CPTs, nos quedamos con el 
						* singular_name asociado a ese objeto. Luego guardamos el string en una variable,
						* y lo usamos en el switch para hacer el Padre de cada CPT.
						*/

						$post = get_queried_object();
						$postType = get_post_type_object(get_post_type($post));
						$obj = $postType->labels->singular_name;   //string del singular_name. Nota: Primer caracter en mayuscula.

						// wpml_current_language – Get the current display language
						$idioma_actual = apply_filters( 'wpml_current_language', NULL );

						// Valores del switch según el singular_name anterior para mostrar el Padre según cada CPT.
						switch ($obj) {			
							case 'Dato':
								//echo "&nbsp;&raquo;&nbsp;";
								echo "<span class='separator'>&raquo;</span>";
								if ($idioma_actual == 'es'): printf (' <a href="' . esc_url( home_url( '/datos/' ) ) . '" title="Datos BNElab">DATOS</a> ');	endif;
								if ($idioma_actual == 'en'): printf (' <a href="' . esc_url( home_url( '/en/data/' ) ) . '" title="Datasets BNElab">DATA</a> ');	endif;	
								break;
							default:	break; //exit switch for all, without Father Breadcumb for CPTs.
						}//switch

						?>
						</div>		
					</header><!-- .entry-header -->
					<div class="entry-content"><?php // ** Plantilla usada para mostrar el contenido de las Taxonomias asociadas al CPT Dato 
					get_template_part('inc/taxonomy-content','dato'); ?>

					<?php the_content(); ?>
					</div><!-- .entry-content home-->
						<?php get_template_part('inc/page','social'); ?>	
				</article><!-- #post-## -->

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>