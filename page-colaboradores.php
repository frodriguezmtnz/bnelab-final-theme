 <?php
/**
 * The template used for displaying page content from page-colaboradores.php
 *
 * Template Name: Template Colaboradores (BNElab)
 * @author 	Felipe Rodríguez (Serikat)
 * @version 9.0
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php 
			/* Plantilla usada para la Información y Perfiles de contacto de los Colaboradores BNElab.
			*  Haciendo uso del ACF Módulo: Colaboradores BNElab.
			*/
			get_template_part('inc/content','colaboradores'); ?>

		</main><!-- #main -->
	</div><!-- #primary -->	
<?php get_sidebar(); ?>
<?php get_footer(); ?>