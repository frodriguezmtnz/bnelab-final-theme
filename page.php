<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package Magnus
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<a href="#top" id="toTop">Subir arriba</a>
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); 
			
				// Depending of type Page (Slug), show default content-page or template part
				$id = get_queried_object()->post_name;  //get slug
				//echo $id;
				switch ($id) {
					case 'proyectos':
						get_template_part('content','proyectos');
						break; //exit id->page='proyectos'
					default: 
						get_template_part( 'content', 'page' ); //Load default template part for PAGES
						break; //exit switch
				}
			?>
			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->
	
	<?php
		// Load only EXTRA code for CPT or whatever, for specifics pages 
		// Depending of type Page (Slug), show default content-page or template part
		$id = get_queried_object()->post_name;  //get slug id_page
		switch ($id) {			 // according slug_id...
			case 'proyectos':    // Custom Template code for 3 of them
			case 'datos':
			case 'herramientas':
				get_template_part('inc/destacados','bnelab');   // custom template for feature list cpt
				break; //exit id->page='proyectos, datos or herramientas'
			default: 
				break; //exit switch for all, without custom template
		}
	?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>