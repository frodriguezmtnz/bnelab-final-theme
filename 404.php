<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Magnus
 */

get_header(); ?>

	
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Error 404', 'bnelab' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">

				<?php
				// wpml_current_language – Get the current display language
				$idioma_actual = apply_filters( 'wpml_current_language', NULL );

				// Mensaje personalizado para mostrar el Error 404.
				switch ($idioma_actual) :
					case 'es': 	echo "<p>" . 'Lo sentimos, la página solicitada no existe. Intenta encontrarlo usando el buscador, o volviendo a la página principal.' . "</p>";	break;
					case 'en': 	echo "<p>" . 'Sorry, the requested page does not exist. Try to find it using the search engine, or by going back to the main page.' . "</p>";	break;
					default:	break;
				endswitch;				

				?>
				<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="BNElab HOME"><i class="fa fa-home fa-3" aria-hidden="true"></i>  BNElab &raquo;</a></p>

				<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->
		</main><!-- #main -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>