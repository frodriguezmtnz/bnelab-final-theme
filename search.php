<?php
/**
 * The template for displaying search results pages.
 *
 * @package Magnus
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
			<?php 
			// ** Custom text for Results searching ** 			
			$idioma_actual = apply_filters( 'wpml_current_language', NULL ); // wpml_current_language – Get the current display language

			if ($idioma_actual == 'es'): ?>
				<h1 class="page-title"><?php printf( __( 'Resultados para: %s', 'bnelab' ), '<span class="keyword">' . get_search_query() . '</span>' ); ?></h1>
			<?php endif;

			if ($idioma_actual == 'en'): ?>
				<h1 class="page-title"><?php printf( __( 'Results for search: %s', 'bnelab' ), '<span class="keyword">' . get_search_query() . '</span>' ); ?></h1>
			<?php endif; ?>

			</header><!-- .page-header -->

		<div class="scroll"><!-- ELIMINAR ??? -->
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>
				<?php
				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
					get_template_part( 'content', 'search' );
				?>
		<?php 	endwhile;

			// ** Navegación y paginación del search.php  Dicha funciona está en el 'functions.php' **
			bnelab_pagination_search();
		?>
		</div><!-- ELIMINAR jscroll.jquery ??? -->		
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>