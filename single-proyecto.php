<?php
/**
 * The template for displaying all single posts.
 *
 * @package Magnus
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

					<div class="entry-meta">
						<?php 
						// TO-DO: Breadcrumb + hierarchy correctly for CPTs
						magnus_posted_on();

						echo ("custom template"); ?><br />
						<?php //echo get_hansel_and_gretel_breadcrumbs(); ?>

						<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
						    <?php // TO-DO: Eliminar este codigo del plugon navxtv breadcrumb
						    if(function_exists('bcn_display')) { bcn_display(); }?>
						</div>
						<?php //magnus_posted_on(); ?>
					</div><!-- .entry-meta -->
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php the_content(); ?>
				</div><!-- .entry-content -->
			</article><!-- #post-## -->

			<?php 
			/* Función personalizada para mostrar la Navegación entre artículos: anterior y siguiente.
			* He reescrito la función del core de wordpress para adaptarlo a las necesidades del proyecto.
			*/
				bnelab_the_post_navigation();
			?>

		<?php endwhile; // end of the loop. ?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>