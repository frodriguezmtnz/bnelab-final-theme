<?php
/**
 * The template for displaying custom Taxonomies for BNElab:
 * licencia, formato, material.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>
<?php 
// Search & Filter for custom Taxonomies
// wpml_current_language – Get the current display language
$idioma_actual = apply_filters( 'wpml_current_language', NULL );
// Depends on language, show one shortcode or other
switch ($idioma_actual) {
	case 'es':
		echo do_shortcode( '[searchandfilter taxonomies="licencia,material,formato" types="checkbox,checkbox,checkbox" headings="Licencia,Material,Formato" empty_search_url="http://nwww.bne.es/labdev/datos/" hide_empty="1,1,1" submit_label="Filtrar"]' );
		break;
	case 'en':
		echo do_shortcode( '[searchandfilter taxonomies="licencia,material,formato" types="checkbox,checkbox,checkbox" headings="Licenses,Material,File Format" empty_search_url="http://nwww.bne.es/labdev/en/data/" hide_empty="1,1,1" submit_label="Filter"]' );
		break;
	default: break;
} ?>
			<?php /* Start the Loop */ 
			while ( have_posts() ) : the_post(); ?>
			<a class="search-href" title="<?php echo the_title();?> | BNElab" href="<?php esc_url( the_permalink() ); ?> " rel="bookmark">
				<article data-aos="fade-up" data-aos-anchor-placement="bottom-bottom" data-aos-duration="800" data-aos-once="true" id="post-<?php the_ID(); ?>" class="hentry search">

					<header class="entry-header">
						<h2 class="entry-title taxo"><?php the_title(); ?><i class="fa fa-link search"></i></h2>
					</header><!-- .entry-header -->

					<div class="entry-content">
						<?php
							/* translators: %s: Name of current post */
							the_excerpt( sprintf(
								__( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'bnelab' ),
								the_title( '<span class="screen-reader-text">"', '"</span>', false )
							) );
						?>
					</div><!-- .entry-content -->
					<footer class="entry-footer search"></footer><!-- .entry-footer -->
				</article><!-- #post-## -->
			</a>

			<?php endwhile; ?>

			<?php //the_posts_navigation(); 
			bnelab_pagination_search(); ?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>