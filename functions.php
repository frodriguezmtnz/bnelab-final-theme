<?php
/**
 * BNElab Funciones, hooks, CPTs, taxonomias y personalizaciones de WP
 * para el bnelab-theme. 
 *
 * @author  Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */

/* Soporte para SVG en upload/biblioteca para Wordpress (BNElab) */
function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );

    return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

// Eliminando CSS adicional del personalizador (BNElab theme)
add_action( 'customize_register', 'ft_customize_register' );

function ft_customize_register( $wp_customize ) {

    $wp_customize->remove_section( 'custom_css' ); //seccion entera fuera
}


/**
* Filters the given oEmbed HTML to make sure it has a title.
*
* @since x.x.x
*
* @param string $result The oEmbed HTML result.
* @param object $data   A data object result from an oEmbed provider.
* @param string $url    The URL of the content to be embedded.
* @return string The filtered oEmbed result.
*/
function wp_filter_oembed_title( $result, $data, $url ) {

    // Get title from oEmbed data to start.
    $title = ! empty( $data->title ) ? $data->title : '';

    // If no oEmbed title, search the return markup for a title attribute.
    $preg_match = '/title\=[\"|\\\']{1}([^\"\\\']*)[\"|\\\']{1}/i';
    $has_title_attr = preg_match( $preg_match, $result, $matches );
        if ( $has_title_attr && ! empty( $matches[1] )) {
            $title = $matches[1];
        }

    $title = apply_filters( 'oembed_title', $title, $result, $data, $url );

    /*
     * If the title attribute already
     * exists, replace with new value.
     *
     * Otherwise, add the title attribute.
     */
    if ( $has_title_attr ) {
        $result = preg_replace( $preg_match, 'title="' . esc_attr( $title ) . '"', $result );
    } else {
        $result = preg_replace( '/^\<iframe/i', '<iframe title="' . esc_attr( $title ) . '"', $result );
    }

    return $result;
}
add_filter( 'oembed_dataparse', 'wp_filter_oembed_title', 9, 3 );


// Hook para ocultar las actualizaciones de WP en el footer derecho, solo para Admins.
function footer_core_wp_hide_bnelab () {
    if ( ! current_user_can('update_core') ) {   // only for admins
        remove_filter( 'update_footer', 'core_update_footer' ); 
    }
}
add_action( 'admin_menu', 'footer_core_wp_hide_bnelab' );


// remove links/menus from the admin bar
function admin_bar_remove_sections_bnelab() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
    $wp_admin_bar->remove_menu('updates');
}
add_action( 'wp_before_admin_bar_render', 'admin_bar_remove_sections_bnelab' );

// Hook para personalizar el logotipo de BNElab en el backend de Wordpress
function custom_logo_bnelab_backend() {
echo '
<style type="text/css">
.wp-admin #wpadminbar #wp-admin-bar-site-name>.ab-item:before {
background-image: url(' . get_bloginfo('stylesheet_directory') . '/images/backend-logo-bnelab.png) !important;
background-position: 0 0;
color:rgba(0, 0, 0, 0);
width: 32px;
height: 32px;
}
#wpadminbar #wp-admin-bar-wp-logo.hover > .ab-item .ab-icon {
background-position: 0 0;
}
</style>
';
}
add_action('wp_before_admin_bar_render', 'custom_logo_bnelab_backend'); //hook into the administrative header output

// Hook para el nodo y logotipo de WP en el backend de Wordpress
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );
function remove_wp_logo( $wp_admin_bar ) {
    $wp_admin_bar->remove_node( 'wp-logo' );
}


// Hook Personalizar frase admin footer Wordpress :)
function modify_footer_admin_bnelab () {
  echo 'Desarrollado con <i class="icon ion-heart"></i> y WordPress por <a target="_blank" href="http://serikat.es/">Serikat.</a>';
}
add_filter('admin_footer_text', 'modify_footer_admin_bnelab');


/* WPML Menu Fix */
add_filter( 'wp_nav_menu_items', 'language_selector_special_2',10,2 );
function language_selector_special_2( $items,$args ){
    $languages  = icl_get_languages('skip_missing=0&orderby=code');
    $ls_str     = NULL;
 
    if( !empty($languages)  && $args->theme_location == '' ){
           
        foreach($languages as $l){
                        $ls_str .= "<a href='{$l['url']}' class='a-class-lang-code-{$l['language_code']}'>";
                        $ls_str .= $l['language_code'];
                        $ls_str .= "</a>";
                        $ls_str .= ' / ';
        }
                   
        $ls_str = $items . '<li class="langs menu-item">'.substr($ls_str,0,-2).'</li>';
  
        return $ls_str;
    }
   
    return $items;
}


/* Custom output for the_content <p> paragraph & AOS - Animate On Scroll Library */
function custom_p_content($content) {
    $content = str_replace('<p>','<p data-aos="fade-up" data-aos-duration="800" data-aos-once="true">', $content);

    return $content;
}
add_filter('the_content','custom_p_content');


/**
 * Remove empty paragraphs created by wpautop()
 * @author Ryan Hamilton
 * @link https://gist.github.com/Fantikerz/5557617
 */
function remove_empty_p( $content ) {
    $content = force_balance_tags( $content );
    $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
    $content = preg_replace( '~\s?<p>(\s| )+</p>\s?~', '', $content );
    return $content;
}
add_filter('the_content', 'remove_empty_p', 20, 1);


/* ACF Object-Post -> Ocultar post/ítems en borrador, privado, etc.
 * Sólo se muestra los publicados. Usado para el módulo "Módulo: Datos relacionados y contacto (taxonomías)"
*/
function only_show_publish_acf_custom_bnelab1($options, $field, $the_post) {
$options['post_status']  = array('publish');
$options['post__not_in'] = array( $the_post );

return $options;
}
add_filter('acf/fields/post_object/query/name=elegir-datos-relacionados', 'only_show_publish_acf_custom_bnelab1', 10, 3);

/* ACF Object-Post -> Ocultar post/ítems en borrador, privado, etc.
 * Sólo se muestra los publicados. Usado para el módulo "Módulo: Destacar proyectos, herramientas y/o datos + Cadena Texto (HOME BNElab)"
*/
function only_show_publish_acf_custom_bnelab2($options, $field, $the_post) {
$options['post_status']  = array('publish');
$options['post__not_in'] = array( $the_post );

return $options;
}
add_filter('acf/fields/post_object/query/name=destacar-proyectos-homebnelab', 'only_show_publish_acf_custom_bnelab2', 10, 3);

/* ACF Object-Post -> Ocultar post/ítems en borrador, privado, etc.
 * Sólo se muestra los publicados. Usado para el módulo "Módulo: Listado de Datos (BNElab)"
*/
function only_show_publish_acf_custom_bnelab3($options, $field, $the_post) {
$options['post_status']  = array('publish');
$options['post__not_in'] = array( $the_post );

return $options;
}
add_filter('acf/fields/post_object/query/name=datos-bnelab', 'only_show_publish_acf_custom_bnelab3', 10, 3);

/* ACF Object-Post -> Ocultar post/ítems en borrador, privado, etc.
 * Sólo se muestra los publicados. Usado para el módulo "Módulo: Listado de Herramientas (BNElab)"
*/
function only_show_publish_acf_custom_bnelab4($options, $field, $the_post) {
$options['post_status']  = array('publish');
$options['post__not_in'] = array( $the_post );

return $options;
}
add_filter('acf/fields/post_object/query/name=herramientas-bnelab', 'only_show_publish_acf_custom_bnelab4', 10, 3);

/* ACF Object-Post -> Ocultar post/ítems en borrador, privado, etc.
 * Sólo se muestra los publicados. Usado para el módulo "Módulo: Listado de Proyectos (BNElab)"
*/
function only_show_publish_acf_custom_bnelab5($options, $field, $the_post) {
$options['post_status']  = array('publish');
$options['post__not_in'] = array( $the_post );

return $options;
}
add_filter('acf/fields/post_object/query/name=proyectos-bnelab', 'only_show_publish_acf_custom_bnelab5', 10, 3);



/**
 * Remove "Multilingual Content Setup" meta box
 *
 * @source: //wpml.org/forums/topic/how-to-remove-ml-content-metabox/
 */
add_action( 'admin_head', 'so_remove_wpml_meta_box' );
function so_remove_wpml_meta_box() {
    $screen = get_current_screen();
    remove_meta_box( 'icl_div_config', $screen->post_type, 'normal' );
}


/*  Custom rel=prev rel=next for Archives pages */
function rel_next_prev(){
    global $paged;

    if (is_archive()):

        if ( get_previous_posts_link() ) { ?>
            <link rel="prev" href="<?php echo get_pagenum_link( $paged - 1 ); ?>" /><?php
        }

        if ( get_next_posts_link() ) { ?>
            <link rel="next" href="<?php echo get_pagenum_link( $paged +1 ); ?>" /><?php
        }
    endif;
}
add_action( 'wp_head', 'rel_next_prev' );


/* Custom url for searching on wordpress */
function change_search_url_rewrite() {

    if ( is_search() && ! empty( $_GET['s'] ) ){
        wp_redirect( home_url( "/search/" ) . urlencode( get_query_var( 's' ) ) );
        exit();
    }        
}
add_action( 'template_redirect', 'change_search_url_rewrite' );


/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );


/**
 * Display a Dropdown Filter Taxonomy custom in admin by CPT dato
 */
function bnelab_multiple_filter_at_cpt() {
    
    if (is_admin()):
        global $typenow;
        $args       = array(
            'public'   => true,
            '_builtin' => true,
        );
        $custom_post_types = get_post_types( $args );
        $custom_post_types = array_values( $custom_post_types );

        if( empty( $typenow ) || in_array( $typenow, $custom_post_types ) === true )
            return;

        $taxonomies = get_object_taxonomies( $typenow );
        $post_type = get_post_type();

        if ( $typenow == $post_type ) {

            foreach ( $taxonomies as $tax ) {

                if ( ! empty( $tax ) && ! is_wp_error( $tax ) ) {

                    $selected = isset( $_GET[ $tax ] ) ? $_GET[ $tax ] : '';
                    $info_taxonomy = get_taxonomy( $tax );

                    wp_dropdown_categories( array(
                        'show_option_all' => __( "Filtrar por: {$info_taxonomy->label}" ),
                        'taxonomy'        => $tax,
                        'name'            => $tax,
                        'orderby'         => 'name',
                        'selected'        => $selected,
                        'show_count'      => true,
                        'hide_empty'      => true,
                    ) );

                }

            }
        }
    endif;
}
add_action( 'restrict_manage_posts', 'bnelab_multiple_filter_at_cpt' );

/**
 * Set multiple filter at custom post type. And fire.
 *
 * @param $query
 */
function bnelab_multiple_filter_taxonomy_term( $query ) {

    if (is_admin()):
        global $pagenow, $typenow;
        $taxonomies = get_object_taxonomies( $typenow );
        $post_type = $query->query['post_type'];

        if ( ! empty( $taxonomies ) && ! is_wp_error( $taxonomies ) ) {

            foreach ( $taxonomies as $tax ) {

                if ( ! empty( $tax ) ) {

                    $q_vars = &$query->query_vars;
                    if ( $pagenow == 'edit.php'
                         && isset( $q_vars[ 'post_type' ] )
                         && $q_vars[ 'post_type' ] == $post_type
                         && isset( $q_vars[ $tax ] )
                         && is_numeric( $q_vars[ $tax ] )
                         && $q_vars[ $tax ] != 0
                    ) {

                        $term           = get_term_by( 'id', $q_vars[ $tax ], $tax );
                        $q_vars[ $tax ] = $term->slug;

                    }

                }

            }

        }
    endif;
}
add_filter( 'parse_query', 'bnelab_multiple_filter_taxonomy_term' );



/**
 * Remove <meta name="generator"> tag created by the WPML PLugin. 
 * Wrapped in an if-statement that checks for an instance of the 
 * sitepress-class (made global by WPML)
 *
 * @url http://wordpress.stackexchange.com/questions/117469/how-to-remove-wpml-generator-meta-tag-by-themes-functions-php-override-plugin
 * 
 * @uses add_action
 * @uses remove_action
 * @uses current_filter
 */
if ( !empty ( $GLOBALS['sitepress'] ) ) {

    function remove_wpml_generator() {
    
        remove_action(
            current_filter(),
            array ( $GLOBALS['sitepress'], 'meta_generator_tag' )
        );
    
    }
    add_action( 'wp_head', 'remove_wpml_generator', 0 );
    
} 

/* Redirección del archive para los CPTs de BNElab: proyecto, herramienta y dato.
*  Según el idioma de la página web, redireccionamos al padre.
*/

function redirect_cpt_archive() {

    // Según el idioma, vemos el post_type del CPT, y se redirecciona al padre en su idioma
    $idioma_actual = apply_filters( 'wpml_current_language', NULL );

    //switch ($idioma_actual) {
    switch (ICL_LANGUAGE_CODE){
        // Idioma Español
        case 'es':
            if( is_post_type_archive( 'proyecto' ) )    {   wp_redirect( home_url( '/proyectos/' ),    301 );  exit(); }
            if( is_post_type_archive( 'herramienta' ) ) {   wp_redirect( home_url( '/herramientas/' ), 301 );  exit(); }
            if( is_post_type_archive( 'dato' ) )        {   wp_redirect( home_url( '/datos/' ),        301 );  exit(); }
            break;             
        // Idioma Inglés
        case 'en':
            if( is_post_type_archive( 'proyecto' ) )    {   wp_redirect( home_url( '/en/projects/' ),  301 );  exit(); }
            if( is_post_type_archive( 'herramienta' ) ) {   wp_redirect( home_url( '/en/tools/' ),     301 );  exit(); }
            if( is_post_type_archive( 'dato' ) )        {   wp_redirect( home_url( '/en/data/' ),      301 );  exit(); }
            break;
        default:    break;
    }
}
add_action( 'template_redirect', 'redirect_cpt_archive' );

/* Eliminación de etiquetas en el <head> innecesarias para WP 
*************************************************************
*/
remove_action ('parse_request', 'rest_api_loaded'); // silently turns off output of user info
remove_action ('wp_head', 'wlwmanifest_link');   //removes wlwmanifest (Windows Live Writer) link
remove_action ('wp_head', 'wp_generator');   //removes meta name generator
remove_action ('wp_head', 'wp_shortlink_wp_head');   //removes shortlink.
remove_action ('wp_head', 'rsd_link');   //removes EditURI/RSD (Really Simple Discovery) link
remove_action ('wp_head', 'adjacent_posts_rel_link_wp_head', 10);    //removes prev and next links
add_filter ('the_generator', '__return_false');
add_filter ('show_admin_bar','__return_false'); // mostrar,ocultar barra admin
remove_action ( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action ( 'wp_print_styles', 'print_emoji_styles' );
remove_action ( 'wp_head', 'feed_links', 2 ); //removes feed links
remove_action ('wp_head', 'feed_links_extra', 3 );  //removes comments feed
remove_action ('wp_head', 'wp_resource_hints', 2);

// Remove All Yoast HTML Comments
// https://gist.github.com/paulcollett/4c81c4f6eb85334ba076
add_action ('wp_head',function() { ob_start(function($o) {
return preg_replace('/^\n?<!--.*?[Y]oast.*?-->\n?$/mi','',$o);
}); },~PHP_INT_MAX);



/* Funcion para hacer la paginación en los resultados de Busqueda (search.php)
*  Dicha función va almacenando los valores totales de las páginas de resultados.
*  + siguiente y anterior página
*  + primero y última página
*  + actual página
*/
function bnelab_pagination_search ($pages = '', $range = 1) {  
    //https://codex.wordpress.org/Function_Reference/paginate_links
    $showitems = ($range)+1;  //siguiente y anterior pag.

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '') {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages) {
            $pages = 1;
         }
     }

     if(1 != $pages) {
         echo "<div class='pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a rel='prev' href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++) {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )) {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a rel='next' href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}


/**
* Function for removing metabox for PAGES of BNElab backend. 
* @param null, not necesary
*/
function remove_metabox_page_fields_bnelab() {
    remove_meta_box( 'commentstatusdiv' , 'page' , 'normal' );        //removes comments status
    remove_meta_box( 'commentsdiv' , 'page' , 'normal' );             //removes comments
    remove_meta_box( 'authordiv' , 'page' , 'normal' );               //removes author 
    remove_meta_box( 'slugdiv', 'page', 'normal' );                   //removes slug metabox
    remove_meta_box( 'rocket_post_exclude', 'page', 'side' );         //remove ajustes de la cache de Page (wp rocket)
    remove_meta_box( 'rocket_post_exclude', 'post', 'side' );         //remove ajustes de la cache de Post (wp rocket)
    remove_meta_box( 'rocket_post_exclude', 'proyecto', 'side' );     //remove ajustes de la cache de CPT Proyecto (wp rocket)
    remove_meta_box( 'rocket_post_exclude', 'dato', 'side' );         //remove ajustes de la cache de CPT Dato (wp rocket)
    remove_meta_box( 'rocket_post_exclude', 'herramienta', 'side' );  //remove ajustes de la cache de CPT Herramienta (wp rocket)
}
add_action( 'admin_menu' , 'remove_metabox_page_fields_bnelab' );

add_action('do_meta_boxes', 'remove_thumbnail_metabox_bnelab');
function remove_thumbnail_metabox_bnelab() {
    remove_meta_box( 'postimagediv','page','side' );  //removes featured image
}


/**
 * @param array $sizes    An associative array of image sizes.
 * @param array $metadata An associative array of image metadata: width, height, file.
 */
function remove_image_sizes( $sizes, $metadata ) {
    return [];
}
//add_filter( 'intermediate_image_sizes_advanced', 'remove_image_sizes', 10, 2 );

/* 
* Convert relative path from Image libray WP (not absolute)
*/
function switch_to_relative_url($html, $id, $caption, $title, $align, $url, $size, $alt) {
    $imageurl = wp_get_attachment_image_src($id, $size);
    $relativeurl = wp_make_link_relative($imageurl[0]);   
    $html = str_replace($imageurl[0],$relativeurl,$html);
      
    return $html;
}
//add_filter('image_send_to_editor','switch_to_relative_url',10,8);



/* Añade automaticamente a las imagenes subidas a la Biblioteca (Medios) el atributo alt*/
add_action( 'add_attachment', 'add_title_image_attachment_uploads' );

function add_title_image_attachment_uploads( $attachment_ID ) {

    $filename   =   $_REQUEST['name']; // or get_post by ID
    $withoutExt =   preg_replace('/\\.[^.\\s]{3,4}$/', '', $filename);
    $withoutExt =   str_replace(array('-','_'), ' ', $withoutExt);

    $my_post = array(
        'ID'           => $attachment_ID,
        //'post_excerpt' => $withoutExt,  // caption
        //'post_content' => $withoutExt,  // description
    );
    wp_update_post( $my_post );

    // update alt text for post
    update_post_meta($attachment_ID, '_wp_attachment_image_alt', $withoutExt );
}

# -------------------------------------------------------
# REMOVE SCREEN READER TEXT FROM POST PAGINATION & H2 Tag
# -------------------------------------------------------
function sanitize_pagination($content) {
    // Remove h2 tag
    $content = preg_replace('#<h2.*?>(.*?)<\/h2>#si', '', $content);
    return $content;
}
add_action('navigation_markup_template', 'sanitize_pagination');


/**
 * Retrieves the navigation to next/previous post, when applicable.
 *
 * @since 4.1.0
 * @since 4.4.0 Introduced the `in_same_term`, `excluded_terms`, and `taxonomy` arguments.
 *
 * @param array $args {
 *     Optional. Default post navigation arguments. Default empty array.
 *
 *     @type string       $prev_text          Anchor text to display in the previous post link. Default '%title'.
 *     @type string       $next_text          Anchor text to display in the next post link. Default '%title'.
 *     @type bool         $in_same_term       Whether link should be in a same taxonomy term. Default false.
 *     @type array|string $excluded_terms     Array or comma-separated list of excluded term IDs. Default empty.
 *     @type string       $taxonomy           Taxonomy, if `$in_same_term` is true. Default 'category'.
 *     @type string       $screen_reader_text Screen reader text for nav element. Default 'Post navigation'.
 * }
 * @return string Markup for post links.
 */
function get_bnelab_the_post_navigation( $args = array() ) {
    $args = wp_parse_args( $args, array(
        'prev_text'          => '&laquo; %title',
        'next_text'          => '%title &raquo;',
        'in_same_term'       => false,
        'excluded_terms'     => '',
        'taxonomy'           => 'category',
        'screen_reader_text' => __( 'Post navigation' ),
    ) );

    $navigation = '';

    $previous = get_previous_post_link(
        '<div class="nav-previous">%link</div>',
        $args['prev_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );

    $next = get_next_post_link(
        '<div class="nav-next">%link</div>',
        $args['next_text'],
        $args['in_same_term'],
        $args['excluded_terms'],
        $args['taxonomy']
    );

    // Only add markup if there's somewhere to navigate to.
    if ( $previous || $next ) {
        $navigation = _navigation_markup( $previous . $next, 'post-navigation', $args['screen_reader_text'] );
    }

    return $navigation;
}

/**
 * Displays the navigation to next/previous post, when applicable.
 *
 * @since 4.1.0
 *
 * @param array $args Optional. See get_the_post_navigation() for available arguments.
 *                    Default empty array.
 */
function bnelab_the_post_navigation( $args = array() ) {
    echo get_bnelab_the_post_navigation( $args );
}


// add custom feed content
// funciona. Quitar o personalizar esta parte del código
/*
function add_feed_content($content) {
    if(is_feed()) {
        $content .= '<p>This article is copyright &copy; '.date('Y').'&nbsp;'.bloginfo('name').'</p>';
    }
    return $content;
}
add_filter('the_excerpt_rss', 'add_feed_content');
add_filter('the_content', 'add_feed_content');
*/


function limitar_palabras($string, $word_limit) {

    // creates an array of words from $string (this will be our excerpt)
    // explode divides the excerpt up by using a space character
    $words = explode(' ', $string);

    // this next bit chops the $words array and sticks it back together
    // starting at the first word '0' and ending at the $word_limit
    // the $word_limit which is passed in the function will be the number
    // of words we want to use
    // implode glues the chopped up array back together using a space character
    return implode(' ', array_slice($words, 0, $word_limit));
}


/**
* Funcion que recorta el titulo de los articulos, etc., para poner "Titulo ejemplo..." "..." al final
* */
function the_title_recortado($before = '', $after = '', $echo = true, $length = false) 
  {
    $title = get_the_title();

    if ( $length && is_numeric($length) ) {
        $title = substr( $title, 0, $length );
    }

    if ( strlen($title)> 0 ) {
        $title = apply_filters('the_title_recortado', $before . $title . $after, $before, $after);
        if ( $echo )
            echo $title;
        else
            return $title;
    }
}


/**
* Funcion que recorta el titulo de los articulos, etc., para poner "Titulo ejemplo..." "..." al final
* */
function resumen_recortado($before = '', $after = '', $echo = true, $length = false) 
  {
    $title = get_the_excerpt();

    if ( $length && is_numeric($length) ) {
        $title = substr( $title, 0, $length );
    }

    if ( strlen($title)> 0 ) {
        $title = apply_filters('the_title_recortado', $before . $title . $after, $before, $after);
        if ( $echo )
            echo $title;
        else
            return $title;
    }
}

/* Check image resolution before upload image to gallery
****************************************************** */
//add_filter('wp_handle_upload_prefilter','mdu_validate_image_size');
function mdu_validate_image_size( $file ) {

    //$currentScreen = get_current_screen();
    //if( $currentScreen->post_type == 'dato' ) {
    if(get_post_type($_REQUEST['post_id']) == ('dato' || 'herramienta' || 'proyecto') ){

        $image = getimagesize($file['tmp_name']);
        $minimum = array(
            'width' => '1024',
            'height' => '537'
        );
        $maximum = array(
            'width' => '1280',
            'height' => '720'
        );
        $image_width = $image[0];
        $image_height = $image[1];

        $too_small = "¡Error! Imagen demasiado pequeña. Tamaño necesario: {$minimum['width']} x {$minimum['height']} px. Tamaño de la imagen subida: $image_width x $image_height pixels.";
        $too_large = "¡Error! Imagen demasiado grande. Tamaño necesario: {$maximum['width']} x {$maximum['height']} px. Tamaño de la imagen subida: $image_width x $image_height pixels.";

        if ( $image_width < $minimum['width'] || $image_height < $minimum['height'] ) {
            // add in the field 'error' of the $file array the message 
            $file['error'] = $too_small; 
            return $file;
        }
        elseif ( $image_width > $maximum['width'] || $image_height > $maximum['height'] ) {
            //add in the field 'error' of the $file array the message
            $file['error'] = $too_large; 
            return $file;
        }
        else
            return $file;
   }
   return $file;
}

// Add custom post types - cpt1 and cpt2 to main RSS feed.
function mycustomfeed_cpt_feed( $query ) {
        if ( $query->is_feed() )
            $query->set( 'post_type', array( 'page', 'proyecto', 'herramienta', 'dato' ) ); 
        return $query;
}
add_filter( 'pre_get_posts', 'mycustomfeed_cpt_feed' );


if ( ! function_exists( 'bnelab_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function bnelab_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on BNElab, use a find and replace
	 * to change 'bnelab' to the name of your theme in all the template files
	 */
	//load_theme_textdomain( 'bnelab', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	//add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

    // we need to include the file below because the activate_plugin() function isn't normally defined in the front-end
    //include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    // activate pre-bundled plugins for bnelab.bne.es
    //activate_plugin( 'wordpress-seo/wp-seo.php' );
    //activate_plugin( 'advanced-custom-fields/acf.php' );
    //activate_plugin( 'advanced-custom-fields-pro/acf.php' );
    //activate_plugin( 'post-type-requirements-checklist/post-type-requirements-checklist.php' );
    //activate_plugin( 'search-filter/search-filter.php' );
    //activate_plugin( 'sitepress-multilingual-cms/sitepress.php' );
    //activate_plugin( 'wpml-string-translation/plugin.php' );
    //activate_plugin( 'acfml/wpml-acf.php' );    
        
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	//set_post_thumbnail_size( 256, 256, true );
	add_image_size ( 'bnelab-large', 1280, 720, true  );
    add_image_size ( 'bnelab-normal', 1024, 537, true  );
    add_image_size ( 'bnelab-medium', 412, 293, true );
    add_image_size ( 'bnelab-small', 240, 135, true );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
        'primary' => __( 'Primary Sidebar Navigation (BNElab)', 'bnelab-theme' ),
				// 'social'  => __( 'Social Links', 'bnelab' ),
        'secondary' => __( 'Language Navigation (BNElab)', 'bnelab-theme' ),
        'footer_menu' => __('Footer menu links (BNElab)', 'bnelab-theme'),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );
}
endif; // bnelab_setup
add_action( 'after_setup_theme', 'bnelab_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 * Priority 0 to make it available to lower priority callbacks.
 * @global int $content_width
 */
function bnelab_content_width() {
    $GLOBALS['content_width'] = apply_filters( 'bnelab_content_width', 1088 );
}
add_action( 'after_setup_theme', 'bnelab_content_width', 0 );


/* Delete Widget Title from widgets sidebar */
add_filter('widget_title','delete_widget_title'); 
function delete_widget_title($t) { return null; }


/**
 * Register widget area.
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function bnelab_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar lateral (menu BNElab)', 'bnelab-theme' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Añada widgets extras para añadir al menú lateral de BNElab.', 'bnelab-theme' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );

    register_sidebar( array(
        'name'          => __( 'Footer Logos (BNElab)', 'bnelab-theme' ),
        'id'            => 'footer-logos',
        'description'   => __( 'Añada imágenes + urls para los nuevos logotipos del footer de BNElab.', 'bnelab-theme' ),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}
add_action( 'widgets_init', 'bnelab_widgets_init' );


/**
 * JavaScript Detection.
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 * @since bnelab-theme 2.0
 */
function bnelab_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_footer', 'bnelab_javascript_detection', 0 );


/* Custom Login for BNElab (css) */
function custom_login_bnelab() {
    echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/css/custom-login-styles.css" />';
}
add_action('login_head', 'custom_login_bnelab');

/* Custom Login Logo URL BNElab */
function my_login_logo_url() {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'my_login_logo_url' );

function my_login_logo_url_title() {
    return 'BNElab | Biblioteca Nacional de España';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

/**
 * Enqueue scripts and styles ONLY for Front-end Wordpress
 */
function bnelab_scripts() {
	// Load our main stylesheet.
    $style_version = filemtime(get_stylesheet_directory() . '/style.css' );
    wp_enqueue_style( 'bnelab-style', get_template_directory_uri(). '/style.css', '', $style_version);
    // IE 7 support icons
    wp_enqueue_style ('bnelab-ie7-icons', get_template_directory_uri() . '/css/ie7/ie7.css', array(), filemtime( get_stylesheet_directory() . '/css/ie7/ie7.css' ), 'all' );

    //AOS - Animate On Scroll Library
    wp_enqueue_style ( 'bnelab-aos-css', get_template_directory_uri(). '/css/aos.min.css', array(), '2.1.1', 'all' );
    wp_enqueue_script( 'bnelab-aos-js',  get_template_directory_uri() . '/js/aos.min.js', array('jquery'), '2.1.1', true );

    // Politica de Cookies (Cookies Policy) only load JS required
    wp_enqueue_script( 'bnelab-cookies', get_template_directory_uri() . '/js/cookieconsent.min.js', array('jquery'), '3.0.3', true );

    //ProgressBar Efect loading JS (BNElab)
    wp_enqueue_script( 'bnelab-progressbar', get_template_directory_uri() . '/js/pace.min.js', array('jquery'), '1.0.0', true );

    // https://github.com/aFarkas/lazysizes LazyLoad Imagen like Medium
    //wp_enqueue_script( 'bnelab-lazysizes', get_template_directory_uri() . '/js/lazysizes.min.js', array('jquery'), '4.0.2', true );

    //Web Font Loader
    //wp_enqueue_script( 'bnelab-webfontloader', get_template_directory_uri(). '/js/webfontloader.js', array('jquery'), '1.6.26', true );
    wp_enqueue_script( 'bnelab-fontfaceobserver', get_template_directory_uri(). '/js/fontfaceobserver.js', array('jquery'), '2.0.13', true );

    // Load scripts
	wp_enqueue_script( 'bnelab-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), '20120206', true );
    wp_enqueue_script( 'bnelab-scrollprogress', get_template_directory_uri() . '/js/scrollprogress.min.js', array('jquery'), '20172012', true );

    if (is_admin()){
        admin_enqueue_scripts ( 'bnelab-admin-footer-ionicons', '/css/ionicons.min.css', array(), '1.3.3', 'all' );
    }

    if (is_search()){ 
        //wp_enqueue_script( 'bnelab-jquery-jscroll', get_template_directory_uri() . '/js/jquery.jscroll.min.js', array('jquery'), '20120206', true );
    }

    if (is_page('sobre-bnelab') || is_page('about-bnelab') ){
        wp_enqueue_style ('bnelab-swipercss', get_template_directory_uri() . '/css/swiper.css', array(), filemtime( get_stylesheet_directory() . '/css/swiper.css' ), 'all' );
        wp_enqueue_script( 'bnelab-jquery-swiper', get_template_directory_uri() . '/js/swiper.min.js', array('jquery'), filemtime( get_stylesheet_directory() . '/js/swiper.min.js' ), true );
    }

    //global $post; //for searching is_parent
    if ( is_page('equipo') || is_page('colaboradores') || is_page('team') || is_page('collaborators') ) {  //id Page: Colaboradores && father "sobre-bnelab"
        wp_enqueue_style ('bnelab-hintmin', get_template_directory_uri() . '/css/hintmin.min.css', array(), filemtime( get_stylesheet_directory() . '/css/hintmin.min.css' ), 'all' );
        wp_enqueue_script( 'bnelab-jquery-1.8.2', get_template_directory_uri() . '/js/jquery-1.8.2.min.js', array('jquery'), '1.8.2', true );
        wp_enqueue_script( 'bnelab-jquery-popupoverlay', get_template_directory_uri() . '/js/jquery.popupoverlay.min.js', array('jquery'), '1.7.13', true );
    }

	// Javacript functions in BNElab-theme.
	wp_enqueue_script( 'bnelab-script', get_template_directory_uri() . '/js/functions.js', array( 'jquery' ), '20150302', true );
}
add_action( 'wp_enqueue_scripts', 'bnelab_scripts' );

/**
*  Enqueue scripts and styles in ADMIN Back-end Wordpress
*/
function admin_bnelab_scrips(){

    wp_enqueue_style ( 'bnelab-admin-footer-ionicons', get_template_directory_uri() . '/css/ionicons.min.css', array(), filemtime( get_stylesheet_directory() . '/css/ionicons.min.css' ), 'all' );

}
add_action('admin_enqueue_scripts','admin_bnelab_scrips');


function add_defer_attribute($tag, $handle) {
   // add script handles to the array below
   $scripts_to_defer = array('bnelab-cookies', 'bnelab-navigation', 'bnelab-scrollprogress', 'bnelab-swipercss', 'bnelab-aos-css', 'bnelab-ie7-icons');
   
   foreach($scripts_to_defer as $defer_script) {
      if ($defer_script === $handle) {
         return str_replace(' src', ' defer="defer" src', $tag);
      }
   }
   return $tag;
}
//add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);


function add_async_attribute($tag, $handle) {
   // agregar los handles de los scripts en el array
   $scripts_to_async = array('bnelab-aos-css');
    
   foreach($scripts_to_async as $async_script) {
      if ($async_script === $handle) {
         return str_replace(' src', ' async defer src', $tag);
      }
   }
   return $tag;
}
//add_filter('script_loader_tag', 'add_async_attribute', 10, 2);


/**
 * Custom template tags for this theme.  NECESSARY
 */
require get_template_directory() . '/inc/template-tags.php';


/* CPTS y taxonomies para BNElab 
================================= */

function cptui_register_my_cpts() {

    /**
     * Post Type: Proyectos BNElab.
     */
    $labels = array(
        "name" => __( "Proyectos BNElab", "bnelab" ),
        "singular_name" => __( "Proyecto", "bnelab" ),
        "menu_name" => __( "PROYECTOS (BNElab)", "bnelab" ),
        "all_items" => __( "Todos los Proyectos", "bnelab" ),
        "add_new" => __( "Añadir nuevo Proyecto", "bnelab" ),
        "add_new_item" => __( "Añadir nuevo Proyecto", "bnelab" ),
        "edit_item" => __( "Editar Proyecto", "bnelab" ),
        "new_item" => __( "Añadir nuevo Proyecto", "bnelab" ),
        "view_item" => __( "Ver Proyecto publicado", "bnelab" ),
        "view_items" => __( "Ver Proyectos", "bnelab" ),
        "search_items" => __( "Buscar en Proyectos", "bnelab" ),
        "not_found" => __( "Error. Proyecto no encontrado. Intenta otras palabras claves.", "bnelab" ),
        "not_found_in_trash" => __( "Error. Proyecto no encontrado en la papelera. Intenta otras palabras claves.", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
        "featured_image" => __( "Imagen destacada para Proyecto (1024x537px o 1280x720px)", "bnelab" ),
        "set_featured_image" => __( "Establer imagen destacada", "bnelab" ),
        "remove_featured_image" => __( "Eliminar imagen destacada", "bnelab" ),
        "use_featured_image" => __( "Usar imagen destacada", "bnelab" ),
        "archives" => __( "Archivos", "bnelab" ),
        "insert_into_item" => __( "Insertar en la página", "bnelab" ),
        "uploaded_to_this_item" => __( "Subido a este elemento", "bnelab" ),
        "filter_items_list" => __( "Filtrar por la lista de elementos", "bnelab" ),
        "items_list" => __( "Lista de proyectos", "bnelab" ),
        "attributes" => __( "Atributos de Proyectos", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
    );

    $args = array(
        "label" => __( "Proyectos BNElab", "bnelab" ),
        "labels" => $labels,
        "description" => "Proyectos relacionados con BNElab",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "proyecto", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 10,
        "menu_icon" => "dashicons-megaphone",
        "supports" => array( "title", "editor", "thumbnail", "excerpt" ),
    );

    register_post_type( "proyecto", $args );


    /**
     * Post Type: Datos BNElab.
     */
    $labels = array(
        "name" => __( "Datos BNElab", "bnelab" ),
        "singular_name" => __( "Dato", "bnelab" ),
        "menu_name" => __( "Datos (BNElab)", "bnelab" ),
        "all_items" => __( "Todos los Datos", "bnelab" ),
        "add_new" => __( "Añadir nuevo Dato", "bnelab" ),
        "add_new_item" => __( "Añadir nuevo Dato", "bnelab" ),
        "edit_item" => __( "Editar Dato", "bnelab" ),
        "new_item" => __( "Añadir nuevo Dato", "bnelab" ),
        "view_item" => __( "Ver Dato publicado", "bnelab" ),
        "view_items" => __( "Ver Datos", "bnelab" ),
        "search_items" => __( "Buscar en Datos", "bnelab" ),
        "not_found" => __( "Error. Dato no encontrado. Intenta otras palabras claves.", "bnelab" ),
        "not_found_in_trash" => __( "Error. Dato no encontrado en la papelera. Intenta otras palabras claves.", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
        "featured_image" => __( "Imagen destacada (1024x537px o 1280x720px)", "bnelab" ),
        "set_featured_image" => __( "Establecer imagen destacada", "bnelab" ),
        "remove_featured_image" => __( "Eliminar imagen destacada", "bnelab" ),
        "use_featured_image" => __( "Usar imagen destacada", "bnelab" ),
        "archives" => __( "Archivos", "bnelab" ),
        "insert_into_item" => __( "Insertar en la página", "bnelab" ),
        "uploaded_to_this_item" => __( "Subido a este elemento", "bnelab" ),
        "filter_items_list" => __( "Filtrar por la lista de elementos", "bnelab" ),
        "items_list" => __( "Lista de datos", "bnelab" ),
        "attributes" => __( "Atributos", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
    );

    $args = array(
        "label" => __( "Datos BNElab", "bnelab" ),
        "labels" => $labels,
        "description" => "Datos relacionados con BNElab",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "dato", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 12,
        "menu_icon" => "dashicons-image-filter",
        "supports" => array( "title", "editor", "thumbnail", "excerpt", "revisions" ),
        "taxonomies" => array( "licencia", "formato", "material" ),
    );

    register_post_type( "dato", $args );


    /**
     * Post Type: Herramientas BNElab.
     */
    $labels = array(
        "name" => __( "Herramientas BNElab", "bnelab" ),
        "singular_name" => __( "Herramienta", "bnelab" ),
        "menu_name" => __( "HERRAMIENTAS (BNElab)", "bnelab" ),
        "all_items" => __( "Todas las Herramientas", "bnelab" ),
        "add_new" => __( "Añadir nueva Herramienta", "bnelab" ),
        "add_new_item" => __( "Añadir nueva Herramienta", "bnelab" ),
        "edit_item" => __( "Editar Herramienta", "bnelab" ),
        "new_item" => __( "Añadir nueva Herramienta", "bnelab" ),
        "view_item" => __( "Ver Herramienta publicada", "bnelab" ),
        "view_items" => __( "Ver Herramientas", "bnelab" ),
        "search_items" => __( "Buscar en Herramientas", "bnelab" ),
        "not_found" => __( "Error. Herramienta no encontrada. Intenta otras palabras claves.", "bnelab" ),
        "not_found_in_trash" => __( "Error. Herramienta no encontrado en la papelera", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
        "featured_image" => __( "Imagen destacada para Herramienta (1024x537px o 1280x720px)", "bnelab" ),
        "set_featured_image" => __( "Establecer imagen destacada", "bnelab" ),
        "remove_featured_image" => __( "Eliminar imagen destacada", "bnelab" ),
        "use_featured_image" => __( "Usar imagen destacada", "bnelab" ),
        "archives" => __( "Archivos", "bnelab" ),
        "insert_into_item" => __( "Insertar en la página", "bnelab" ),
        "uploaded_to_this_item" => __( "Subido a este elemento", "bnelab" ),
        "filter_items_list" => __( "Filtrar por la lista de elementos", "bnelab" ),
        "items_list" => __( "Lista de herramientas", "bnelab" ),
        "attributes" => __( "Atributos", "bnelab" ),
        "parent_item_colon" => __( "Padre", "bnelab" ),
    );

    $args = array(
        "label" => __( "Herramientas BNElab", "bnelab" ),
        "labels" => $labels,
        "description" => "Herramientas relacionados con BNElab",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => true,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "herramienta", "with_front" => true ),
        "query_var" => true,
        "menu_position" => 11,
        "menu_icon" => "dashicons-hammer",
        "supports" => array( "title", "editor", "thumbnail", "excerpt", "revisions" ),
    );

    register_post_type( "herramienta", $args );

}
add_action( 'init', 'cptui_register_my_cpts' );



function cptui_register_my_taxes() {

    /**
     * Taxonomy: Licencias de Uso (BNElab).
     */
    $labels = array(
        "name" => __( "Licencias de Uso (BNElab)", "bnelab" ),
        "singular_name" => __( "Licencia de uso", "bnelab" ),
        "menu_name" => __( "Licencias de uso (categorías)", "bnelab" ),
    );

    $args = array(
        "label" => __( "Licencias de Uso (BNElab)", "bnelab" ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => true,
        "label" => "Licencias de Uso (BNElab)",
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'licencia', 'with_front' => true,  'hierarchical' => true, ),
        "show_admin_column" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "show_in_quick_edit" => true,
    );
    register_taxonomy( "licencia", array( "dato" ), $args );


    /**
     * Taxonomy: Formatos (BNElab).
     */
    $labels = array(
        "name" => __( "Formatos (BNElab)", "bnelab" ),
        "singular_name" => __( "Formato", "bnelab" ),
        "menu_name" => __( "Formatos (categorías)", "bnelab" ),
    );

    $args = array(
        "label" => __( "Formatos (BNElab)", "bnelab" ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => true,
        "label" => "Formatos (BNElab)",
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'formato', 'with_front' => false,  'hierarchical' => true, ),
        "show_admin_column" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "show_in_quick_edit" => true,
    );
    register_taxonomy( "formato", array( "dato" ), $args );


    /**
     * Taxonomy: Tipos de Materiales (BNElab).
     */
    $labels = array(
        "name" => __( "Tipos de Materiales (BNElab)", "bnelab" ),
        "singular_name" => __( "Material", "bnelab" ),
        "menu_name" => __( "Tipos de Materiales (categorías)", "bnelab" ),
    );

    $args = array(
        "label" => __( "Tipos de Materiales (BNElab)", "bnelab" ),
        "labels" => $labels,
        "public" => true,
        "hierarchical" => true,
        "label" => "Tipos de Materiales (BNElab)",
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'material', 'with_front' => false,  'hierarchical' => true, ),
        "show_admin_column" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "show_in_quick_edit" => true,
    );
    register_taxonomy( "material", array( "dato" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes' );
/*fin cpst&taxanomies*/
