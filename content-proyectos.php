<?php
/**
 * The template used for displaying page content only for slug page "PROYECTOS"
 *
 * @package Magnus
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
		
	<div class="entry-content">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			    <?php if(function_exists('bcn_display')) { bcn_display(); }?>
			</div>
		<?php the_content(); ?>

		<?php get_template_part('inc/page','social'); ?>
	</div><!-- .entry-content home-->
</article><!-- #post-## -->