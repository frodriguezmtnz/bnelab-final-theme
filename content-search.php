<?php
/**
 * The template part for displaying results in search pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<a class="search-href" title="<?php echo the_title();?> | BNElab" href="<?php esc_url( the_permalink() ); ?> " rel="bookmark">
<article data-aos="fade-up" data-aos-anchor-placement="bottom-bottom" data-aos-duration="800" data-aos-once="true" id="post-<?php the_ID(); ?>" class="hentry search">
	<header class="entry-header">
		<h3 class="entry-title search"><?php the_title(); ?><i class="fa fa-link search"></i></h3>
	</header><!-- .entry-header -->
	<div class="entry-summary search"><?php // Mostramos el excerpt manual o automatico + limitacion palabras + '... raquo;'
		echo limitar_palabras(get_the_excerpt(), '50');
		echo '... &raquo;'; 
	?></div><!-- .entry-summary -->
	<footer class="entry-footer search"></footer><!-- .entry-footer -->
</article><!-- #post-## -->
</a>