<?php
/**
 * The template for displaying for custom page
 *
 * Template Name: Template Sobre BNElab
 * @author 	Felipe Rodríguez (Serikat)
 * @package Magnus
 * @version 1.0
 */
?>

<?php get_header(); ?>

	<div id="primary" class="content-area">
		<a href="#top" id="toTop">Subir arriba</a>
		<main id="main" class="site-main" role="main">

			<?php echo "Plantilla PAGE custom page-sobre-bnelab.php"; ?>

			<?php get_template_part('inc/content','sobrebnelab'); ?>
		</main><!-- #main -->
	</div><!-- #primary -->	
<?php get_sidebar(); ?>
<?php get_footer(); ?>