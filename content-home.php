<?php
/**
 * @package Magnus
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('section'); ?>>
	
	<div class="section-inner">
		<header class="entry-header">
			<?php //the_title( sprintf( '<h1 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h1>' ); ?>
			<?php if ( 'post' == get_post_type() ) : ?>
			<?php /*
			<div class="entry-meta">
				<?php magnus_posted_on(); ?>
			</div><!-- .entry-meta -->
			*/ ?>	
			<?php endif; ?>
		</header><!-- .entry-header -->
	</div><!-- .section-inner -->

</article><!-- #post-## -->