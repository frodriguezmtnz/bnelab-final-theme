<?php
/**
 * The template used for displaying page content from page.php
 *
 * @author 	Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php 
	// Text-justify center ONLY for home page. ?>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		
		<?php // Cargamos la plantilla asociada a los breadcrumb para cualquier tipo de Page.
			get_template_part('inc/breadcrumb-equipo','colaboradores'); 
		?>			
	</header><!-- .entry-header -->
	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content home-->

<?php
// Módulo de Filtrado para las Taxonomías para los idiomas ES/EN para la página Datos BNElab.

$id = get_queried_object()->post_name;	// get slug id_page
$idioma_actual = apply_filters( 'wpml_current_language', NULL ); // wpml_current_language – Get the current display language

// Filtrado de taxonomías en español
if ( ($idioma_actual == 'es') && ($id == 'datos') ):
	echo do_shortcode( '[searchandfilter taxonomies="licencia,material,formato" types="checkbox,checkbox,checkbox" headings="Licencia,Material,Formato" empty_search_url="http://nwww.bne.es/labdev/datos/" hide_empty="1,1,1" submit_label="Filtrar"]' );
endif;

// Filtrado de taxonomías en inglés
if ( ($idioma_actual == 'en') && ($id == 'data') ):
	echo do_shortcode( '[searchandfilter taxonomies="licencia,material,formato" types="checkbox,checkbox,checkbox" headings="Licenses,Material,File Format" empty_search_url="http://nwww.bne.es/labdev/en/data/" hide_empty="1,1,1" submit_label="Filter"]' );
endif;
?>

	<?php get_template_part('inc/page','social'); ?>	
</article><!-- #post-## -->