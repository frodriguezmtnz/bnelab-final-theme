<?php
/**
 * @package Magnus
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

		<div class="entry-meta">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="BNElab Home"><i class="fa fa-home fa-3" aria-hidden="true"></i>BNElab</a>
<?php
	/* 
	* https://codex.wordpress.org/Function_Reference/get_post_type_object
	* https://wordpress.stackexchange.com/questions/169504/how-to-get-current-get-post-types-name
	* Según el tipo de objeto, ya sea post, page, o CPTs, nos quedamos con el 
	* singular_name asociado a ese objeto. Luego guardamos el string en una variable,
	* y lo usamos en el switch para hacer el Padre de cada CPT.
	*/

	$post = get_queried_object();
	$postType = get_post_type_object(get_post_type($post));
	$obj = $postType->labels->singular_name;   //string del singular_name. Nota: Primer caracter en mayuscula.

	// wpml_current_language – Get the current display language
	$idioma_actual = apply_filters( 'wpml_current_language', NULL );

	// Valores del switch según el singular_name anterior para mostrar el Padre según cada CPT.
	switch ($obj) {			
		case 'Proyecto':
			//echo "&nbsp;&raquo;&nbsp;";
			echo "<span class='separator'>&raquo;</span>";
			if ($idioma_actual == 'es'): printf (' <a href="' . esc_url( home_url( '/proyectos/' ) ) . '" title="Proyectos BNElab">PROYECTOS</a> ');	endif;
			if ($idioma_actual == 'en'): printf (' <a href="' . esc_url( home_url( '/en/projects/' ) ) . '" title="Projects BNElab">PROJECTS</a> ');	endif;
			break;
		case 'Dato':
			//echo "&nbsp;&raquo;&nbsp;";
			echo "<span class='separator'>&raquo;</span>";
			if ($idioma_actual == 'es'): printf (' <a href="' . esc_url( home_url( '/datos/' ) ) . '" title="Datos BNElab">DATOS</a> ');	endif;
			if ($idioma_actual == 'en'): printf (' <a href="' . esc_url( home_url( '/en/data/' ) ) . '" title="Datasets BNElab">DATA</a> ');	endif;	
			break;
		case "Herramienta":
			//echo "&nbsp;&raquo;&nbsp;";
			echo "<span class='separator'>&raquo;</span>";
			if ($idioma_actual == 'es'): printf (' <a href="' . esc_url( home_url( '/herramientas/' ) ) . '" title="Herramientas BNElab">HERRAMIENTAS</a> ');	endif;
			if ($idioma_actual == 'en'): printf (' <a href="' . esc_url( home_url( '/en/tools/' ) ) . '" title="Tools BNElab">TOOLS</a> ');	endif;			
			break;
		default:	break; //exit switch for all, without Father Breadcumb for CPTs.
	}//switch
?>
	<?php 
		// Los CPT Proyectos y Herramientas TIRAN de este template. Es necesario definir nuestro templates para ellos.
		// TO-DO: Pendiente hacerlo!
		magnus_posted_on(); //esta en inc/template_tags.php?>
		</div><!-- .entry-meta -->
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<?php get_template_part('inc/page','social'); ?>

</article><!-- #post-## -->
<?php 
	/* Función personalizada para mostrar la Navegación entre artículos: anterior y siguiente.
	*  He reescrito la función del core de wordpress para adaptarlo a las necesidades del proyecto.
	*/
	bnelab_the_post_navigation();
?>