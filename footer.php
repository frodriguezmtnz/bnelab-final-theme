<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @author  Felipe Rodríguez (Serikat)
 * @package bnelab-theme
 */
?>
	</section><!-- #content -->

	<div class="scrollme footer">
		<div class="animateme" data-when="enter" easing="easein" data-from="1" data-to="0" data-opacity="0" data-rotatex="45">
			<footer id="colophon" class="site-footer" role="contentinfo">
				<?php // Funcionalidad de Añadir logotipos nuevos al Footer de BNElab + SVG --> 
					dynamic_sidebar('footer-logos'); 

					// Añade la funcionalidad de los enlaces RGPD BNElab
                	wp_nav_menu( array( 'theme_location' => 'footer_menu', 'menu_class' => 'footer-links-menu' ) ); 
                ?>                
			</footer><!-- #colophon -->
		</div><!-- #animateme -->
	</div><!-- scrollme footer -->
</div><!-- #page -->

<?php wp_footer(); ?>
  <!-- Library anime on scroll -->
  <script> AOS.init(); </script>
  <script>
        window.onload = function() {
            scrollProgress.set({
                    styles: false,
                    events: false,
                    bottom: true
                });
            window.onresize = scrollProgress.update;
            window.onscroll = scrollProgress.trigger;
        };
  </script>
<?php // Política de Cookies (Policy Law) por idiomas ES/EN
  switch (ICL_LANGUAGE_CODE) {
    case 'es': ?>
  <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup":  { "background": "#403f3f" },
        "button": { "background": "#f1d600" }
      },
      "theme": "edgeless",
      "content": { "message": "Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestra página web. Si continúa navegando, consideramos que acepta su uso.", "dismiss": "¡Entendido!", "link": "Leer más", "href": "<?php echo home_url(); ?>/politica-de-cookies/" }
    })});
  </script>
<?php   break;
    case 'en': ?>
  <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup":  { "background": "#403f3f" },
        "button": { "background": "#f1d600" }
      },
      "theme": "edgeless",
      "content": { "message": "This website uses cookies to ensure you get the best experience on our website. By using our website, you agree to our use of cookies", "href": "<?php echo home_url(); ?>/cookies-policy/" }
    })});
  </script>
<?php   break;
    default:  break;
  } //fin_switch = ICL_LANGUAGE_
?>

<?php // ** Script del slider swiper que funciona SOLO para la Pag. Sobre/About BNElab **
  if (is_page('sobre-bnelab') || is_page('about-bnelab') ): ?>
  <script>
    var swiper = new Swiper('.swiper-container', {
      slidesPerView: 1,
      loop: true,
      speed: 1000,
      grabCursor: true,
      autoplay: {
        delay: 10000,
        disableOnInteraction: false,
      },
      keyboard: {
        enabled: true,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });
  </script>
<?php endif; ?>
<!-- Loading fonts for BNElab -->
<script>
var fontbnelab = new FontFaceObserver('FontBNElab');
var fontRoboto = new FontFaceObserver('Roboto');
var fontReemKufi = new FontFaceObserver('Reem Kufi');
</script>
</body>
</html>