<?php
/**
 * Template Name: Template Front Home (BNElab)
 * @author 	Felipe Rodríguez (Serikat)
 * @package Magnus
 * @version 1.0
 * ONLY show in front page: the TITLE and whole text justify center.
 * TO-DO: Add other stuff like other page features modules.
 */
?>	
	<div class="entry-content home">
		<?php the_content(); ?>

		<?php
		/*
		* Inicializamos los objetos posts, para hacer uso del Módulo ACF Módulo: Destacar proyectos y herramientas (BNElab).
		* En este módulo, tiene campos multiselección. Usamos set_postdata para hacer uso
		* de las funciones de WP core nativas de forma normal.
		* http://codex.wordpress.org/Template_Tags/get_posts#Reset_after_Postlists_with_offset
		*/

		$post_objects = get_field('destacar-proyectos-homebnelab'); //Llamada al módulo para inicializarlo

		if ( !empty($post_objects) ): ?>
		<div class="container-front">

<?php 
	// wpml_current_language – Get the current display language
	$idioma_actual = apply_filters( 'wpml_current_language', NULL );

	// Extraigo el valor del campo "Cadena texto destacados HOME"
	$cadena_texto_home = get_field("cadena-texto-destacados-home");

	// Dependiendo del idioma y si no está vacío la cadena de texto, lo mostramos
	if ( ($idioma_actual == ('es' || 'en')) && !(empty($cadena_texto_home)) ):
		echo '<h3>'.$cadena_texto_home.'</h3>';
	else: // si no, cargamos los textos por defecto según idioma.
		switch ($idioma_actual) {
			case 'es': echo '<h3>Destacados</h3>'; break;
			case 'en': echo '<h3>Featured</h3>';   break;
			default: break;
		}
	endif;
?>			
			<div class="parent">
				<?php
				/* Nos recorremos el array de objetos del CPT, y mostramos mediante,
				flex-box-grid los elementos de forma ordenada según los hijos. Todo por CSS. */
				foreach( $post_objects as $post): // variable must be called $post (IMPORTANT)
		        	setup_postdata($post); //preparamos los datos de listado de proyectos hechos en BNElab
		        		if( has_post_thumbnail() ): 
						$attachment_image = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full'); ?>
						<div class="child">
							<figure class="destacados-home-bnelab">
								<img class="img-fluid" src="<?php echo $attachment_image[0]; ?>" alt="imagen <?php the_title(); ?>" />
									<figcaption>
									    <div class="square"><div></div></div>
									    <h2><?php echo esc_html( the_title() ); ?></h2>
										<p><?php echo limitar_palabras(get_the_excerpt(), '5'); echo "... &raquo;"; ?></p>
									</figcaption>
										<a href="<?php the_permalink(); ?>" title="<?php the_title();?> - <?php bloginfo ('name'); ?>"></a>
							</figure>
						</div>
				<?php endif; endforeach; wp_reset_postdata();?>
		    </div><!-- /.parent -->
		</div><!-- /.container-front -->
		<?php endif; ?>

	<?php //get_template_part('inc/page','social'); ?>		
	</div><!-- .entry-content -->